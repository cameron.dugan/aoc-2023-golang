package main

import (
	"bufio"
	"fmt"
	"os"
	"reflect"
)

func part1(platform [][]rune) [][]rune {
	// Roll north
	for i := range platform {
		for j := range platform[i] {
			if platform[i][j] == '.' {
				// find rolling rock
				for rp := i + 1; rp < len(platform); rp++ {
					if platform[rp][j] == 'O' {
						// pull rock to position
						platform[i][j], platform[rp][j] = platform[rp][j], platform[i][j]
						break
					} else if platform[rp][j] == '#' {
						break
					}
				}
			}
		}
	}
	return platform
}

func part2(platform [][]rune) [][]rune {
	// run 1000000000 cycles
	history := make([][][]rune, 0)
	for c := 0; c < 1000000000*4; c++ {
		// Roll north
		switch c % 4 {
		case 0:
			fmt.Println("North")
			for i := range platform {
				for j := range platform[i] {
					if platform[i][j] == '.' {
						// find rolling rock
						for rp := i + 1; rp < len(platform); rp++ {
							if platform[rp][j] == 'O' {
								// pull rock to position
								platform[i][j], platform[rp][j] = platform[rp][j], platform[i][j]
								break
							} else if platform[rp][j] == '#' {
								break
							}
						}
					}
				}
			}
		case 1:
			fmt.Println("West")
			for i := range platform {
				for j := range platform[i] {
					if platform[i][j] == '.' {
						// find rolling rock
						for rp := j + 1; rp < len(platform[i]); rp++ {
							if platform[i][rp] == 'O' {
								// pull rock to position
								platform[i][rp], platform[i][j] = platform[i][j], platform[i][rp]
								break
							} else if platform[i][rp] == '#' {
								break
							}
						}
					}
				}
			}
		case 2:
			fmt.Println("South")
			for i := len(platform) - 1; i >= 0; i-- {
				for j := range platform[i] {
					if platform[i][j] == '.' {
						// find rolling rock
						for rp := i - 1; rp >= 0; rp-- {
							if platform[rp][j] == 'O' {
								// pull rock to position
								platform[i][j], platform[rp][j] = platform[rp][j], platform[i][j]
								break
							} else if platform[rp][j] == '#' {
								break
							}
						}
					}
				}
			}
		default:
			fmt.Println("East")
			for i := range platform {
				for j := len(platform[i]) - 1; j >= 0; j-- {
					if platform[i][j] == '.' {
						// find rolling rock
						for rp := j - 1; rp >= 0; rp-- {
							if platform[i][rp] == 'O' {
								// pull rock to position
								platform[i][rp], platform[i][j] = platform[i][j], platform[i][rp]
								break
							} else if platform[i][rp] == '#' {
								break
							}
						}
					}
				}
			}
		}
		// on cycle finish:
		if c%4 == 3 {
			fmt.Printf("c: %v\n", c)
			fmt.Printf("%v\n", len(history))

			// early stop we are in infinite loop
			detectedCycle := -1
			for i, cycle := range history {
				if reflect.DeepEqual(cycle, platform) {
					detectedCycle = i
					break
				}
			}

			var tmp [][]rune
			copy(tmp, platform)
			history = append(history, tmp)
			if detectedCycle != -1 {
				fmt.Println("Cycle detected")
				fmt.Printf("detectedCycle: %v\n", detectedCycle)
				fmt.Println("These are the detected ones: ")

				for _, row := range history[detectedCycle] {
					fmt.Println(string(row))
				}
				println()

				for _, row := range platform {
					fmt.Println(string(row))
				}
				println()

				cycleLen := len(history) - detectedCycle
				platform = history[(1000000000%cycleLen)+detectedCycle]
				fmt.Println("Final Platform:")

				for _, row := range platform {
					fmt.Println(string(row))
				}
				println()
				return platform
			}

			// for _, row := range platform {
			// 	fmt.Println(string(row))
			// }
			// println()
		}
	}
	return platform
}

func main() {
	f, _ := os.Open("input.txt")
	s := bufio.NewScanner(f)
	var platform [][]rune

	for s.Scan() {
		platform = append(platform, []rune(s.Text()))
	}

	// platform = part1(platform)
	platform = part2(platform)

	sum := 0
	for i, row := range platform {
		load := len(platform) - i
		circles := 0
		for _, r := range row {
			if r == 'O' {
				circles++
			}
		}
		sum += load * circles
	}
	fmt.Printf("sum: %v\n", sum)
}
